package com.gegajo.s02app.exceptions;

public class UserException extends Exception {

    public UserException(String message) {
        super(message);
    }
}
